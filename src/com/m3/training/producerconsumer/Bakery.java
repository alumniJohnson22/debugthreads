package com.m3.training.producerconsumer;

public class Bakery {

	public static void main(String[] args) throws InterruptedException {
		int traySize = 6;
		int workQuota = 100;
		Tray tray = new Tray(traySize);
		Baker bakerJob = new Baker(tray, workQuota);
		Thread producer = new Thread(bakerJob);
		producer.start();
		System.out.println("Bakery is open for business!");
		Customer customerJob = new Customer(tray, workQuota);
		Thread customer1 = new Thread(customerJob);
		customer1.start();
		Customer customerJob2 = new Customer(tray, workQuota - 50);
		Thread customer2 = new Thread(customerJob2);
		customer2.start();

		Thread.sleep(500);
		producer.interrupt();
		producer.join();
		customer1.interrupt();
		customer1.join();
		customer2.interrupt();
		customer2.join();
		System.out.println(tray);
		System.out.println("Producer state : " + producer.getState() + " Customer state: " + customer1.getState()
				+ " Customer state: " + customer2.getState());

		// producer.join();
		// customer.join();
		// System.out.println(tray);

	}

}
