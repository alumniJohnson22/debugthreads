package com.m3.training.producerconsumer;

public class Cupcake {
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
		return this.getName();
	}
	
}

