package com.m3.training.producerconsumer;

public class Customer implements Runnable {
	private Tray tray;
	private int quota;

	public Customer(Tray tray, int quota) {
		this.tray = tray;
		this.quota = quota;
	}

	@Override
	public void run() {
		while (this.quota > 0) {
			try {
				Cupcake cupcake = tray.remove();

			} catch (InterruptedException e) {
				System.out.println("failed attempt to remove a cupcake on iteration " + this.quota);
				return;
			}
			this.quota--;
		}
	}

}
