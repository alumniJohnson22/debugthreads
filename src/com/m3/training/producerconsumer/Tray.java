package com.m3.training.producerconsumer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Tray {

	private static final int NEXT_CUPCAKE = 0;
	private final int maxSize;
	private List<Cupcake> container;
	private Map<String, Integer> accounting;

	public Tray(int maxSize) {
		this.maxSize = maxSize;
		container = new ArrayList<>();
		accounting = new HashMap<>();
	}

	public Cupcake add(String cupcakeType) throws InterruptedException {
		boolean cupCakeToBake = true;
		while (cupCakeToBake) {
			synchronized (container) {
				if (container.size() >= this.maxSize) {
					container.wait();
					continue;
				}
				Cupcake cupcake = new Cupcake();
				cupcake.setName(cupcakeType);
				if (container.add(cupcake)) {
					container.notify();
					accounting.put(cupcake.getName(), 1);
					System.out.println("Cupcake added " + cupcake);
					return cupcake;
				}
			}
		}
		throw new IllegalStateException("Could not add cupcake to tray.");
	}

	public Cupcake remove() throws InterruptedException {
		boolean cupCakeToSell = true;
		Cupcake cupcake = null;
		while (cupCakeToSell) {
			synchronized (container) {
				if (container.isEmpty()) {
					container.wait();
					continue;
				}
				cupcake = container.remove(NEXT_CUPCAKE);
				accounting.put(cupcake.getName(), 2);
				System.out.println("Cupcake removed " + cupcake);
				container.notify();
				return cupcake;
			}
		}
		throw new IllegalStateException("Could not add cupcake to tray." + cupcake);
	}

	public String toString() {
		// TODO clear up warning
		ArrayList<String> keyArray = new ArrayList(accounting.keySet());
		Collections.sort(keyArray);
		StringBuffer sb = new StringBuffer();
		for (String key : keyArray) {
			sb.append(key + " " + accounting.get(key) + "\n");
		}
		return sb.toString();
	}
	
}
