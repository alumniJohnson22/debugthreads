package com.m3.training.producerconsumer;

public class Baker implements Runnable {
	private Tray tray;
	private int quota;

	public Baker(Tray tray, int quota) {
		this.tray = tray;
		this.quota = quota;
	}

	@Override
	public void run() {
		while (this.quota > 0) {
			try {
				Cupcake cupcake = tray.add(Integer.toString(this.quota));
			} catch (InterruptedException e) {
				System.out.println("failed attempt to bake a cupcake on iteration " + this.quota);
				return;
			}
			this.quota--;
		}
	}

}
